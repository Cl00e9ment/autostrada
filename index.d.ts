import { RequestHandler } from 'express';

declare interface Options {
	fromPathToUrl?: (path: string) => string | string[],
	indexableFilesFilter?: (path: string) => boolean,
	wwwDirectory?: string,
}

declare interface AutostradaMiddleware extends RequestHandler {
	lastChance: (delay: number) => RequestHandler,
}

declare function autostrada(option?: Options): AutostradaMiddleware;

export default autostrada;

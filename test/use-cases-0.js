module.exports = [
	{
		url: '/',
		expect: {
			status: 200,
			txt: 'Content of /index.txt'
		}
	},
	{
		url: '/dir0',
		expect: {
			status: 302,
			txt: 'Found. Redirecting to /dir0/'
		}
	},
	{
		url: '/dir0/',
		expect: {
			status: 200,
			txt: 'Content of /dir0/index.txt'
		}
	},
	{
		url: '/dir0/file0',
		expect: {
			status: 200,
			txt: 'Content of /dir0/file0.txt'
		}
	},
	{
		url: '/hidden',
		expect: {
			status: 404,
			txt: 'Not Found'
		}
	},
	{
		url: '/dir0/non-existent.txt',
		expect: {
			status: 404,
			txt: 'Not Found'
		}
	},
];

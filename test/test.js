const util = require('util');
const exec = util.promisify(require('child_process').exec);
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('./test-server/index.js');
const rimraf = require('rimraf');
const fs = require('fs');
const useCases = [
	require('./use-cases-0.js'),
	require('./use-cases-1.js'),
	require('./use-cases-2.js'),
];

const root = __dirname + '/test-server/www';
const expect = chai.expect;
chai.use(chaiHttp);

let additionalFilesPresent;

async function install() {
	await exec('cd test/test-server && npm i');
}

function testUseCases(useCases, runWithAdditionalFiles) {
	for (const useCase of useCases) {
		describe(`GET ${useCase.url}`, () => {
			it(`should return ${useCase.expect.status}, ${useCase.expect.txt}`, done => {
				if (runWithAdditionalFiles && !additionalFilesPresent) {
					addFiles();
				} else if (!runWithAdditionalFiles && additionalFilesPresent) {
					removeAddedFiles();
				}
				chai.request(app).get(useCase.url).redirects(0).end((_err, res) => {
					expect(res).to.have.status(useCase.expect.status);
					expect(res.text).to.equal(useCase.expect.txt);
					done();
				});
			});
		});
	}
}

async function addFiles() {
	console.log('adding files...');
	fs.mkdirSync(`${root}/dir1`);
	fs.mkdirSync(`${root}/dir1/dir2`);
	fs.mkdirSync(`${root}/.dir3`);
	fs.writeFileSync(`${root}/dir0/file1.txt`, 'Content of /dir0/file1.txt');
	fs.writeFileSync(`${root}/dir1/index.txt`, 'Content of /dir1/index.txt');
	fs.writeFileSync(`${root}/dir1/dir2/index.txt`, 'Content of /dir1/dir2/index.txt');
	fs.writeFileSync(`${root}/.dir3/file0.txt`, 'Content of /.dir3/file0.txt');
	additionalFilesPresent = true;
}

async function removeAddedFiles() {
	console.log('removing added files...');
	rimraf.sync(`${root}/dir0/file1.txt`);
	rimraf.sync(`${root}/dir1`);
	rimraf.sync(`${root}/.dir3`);
	additionalFilesPresent = false;
}

before(function(done) {
	this.timeout(-1);
	removeAddedFiles();
	install().then(done);
});

testUseCases(useCases[1], false);
testUseCases(useCases[0], false);

testUseCases(useCases[2], true);
testUseCases(useCases[0], true);

testUseCases(useCases[1], false);
testUseCases(useCases[0], false);

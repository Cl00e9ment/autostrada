const app = require('express')();
const autostrada = require('../../index.js')({
	wwwDirectory: __dirname + '/www',
	indexableFilesFilter: path => path !== '/hidden.txt',
	fromPathToUrl: (path) => {
        // '/xxx/yyy/index.txt' => ['/xxx/yyy/', '/xxx/yyy']
        if (path.match(/^((.*)\/)index\.txt$/)) return [RegExp.$1, RegExp.$2];
        // '/xxx/yyy/zzz.txt' => '/xxx/yyy/zzz'
        if (path.match(/^(.*)\.txt$/)) return RegExp.$1;
        // no mofification
        return path;
    },
});

app.use(autostrada);
app.use(autostrada.lastChance(150));
app.use((_req, res, _next) => {
	res.status(404).send('Not Found');
});

module.exports = app;

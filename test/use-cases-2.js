module.exports = [
	{
		url: '/dir0/file1',
		expect: {
			status: 200,
			txt: 'Content of /dir0/file1.txt'
		}
	},
	{
		url: '/dir1',
		expect: {
			status: 302,
			txt: 'Found. Redirecting to /dir1/'
		}
	},
	{
		url: '/dir1/',
		expect: {
			status: 200,
			txt: 'Content of /dir1/index.txt'
		}
	},
	{
		url: '/dir1/dir2',
		expect: {
			status: 302,
			txt: 'Found. Redirecting to /dir1/dir2/'
		}
	},
	{
		url: '/dir1/dir2/',
		expect: {
			status: 200,
			txt: 'Content of /dir1/dir2/index.txt'
		}
	},
	{
		url: '/.dir3/file0',
		expect: {
			status: 200,
			txt: 'Content of /.dir3/file0.txt'
		}
	},
];

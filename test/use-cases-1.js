module.exports = [
	{
		url: '/dir0/file1',
		expect: {
			status: 404,
			txt: 'Not Found'
		}
	},
	{
		url: '/dir1',
		expect: {
			status: 404,
			txt: 'Not Found'
		}
	},
	{
		url: '/dir1/',
		expect: {
			status: 404,
			txt: 'Not Found'
		}
	},
	{
		url: '/dir1/dir2',
		expect: {
			status: 404,
			txt: 'Not Found'
		}
	},
	{
		url: '/dir1/dir2/',
		expect: {
			status: 404,
			txt: 'Not Found'
		}
	},
	{
		url: '/.dir3/file',
		expect: {
			status: 404,
			txt: 'Not Found'
		}
	},
];

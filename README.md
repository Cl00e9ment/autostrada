# Autostrada

Autostrada uses Express to auto route the content of a www directory. You can also setup a custom URL for each file.

## Installation

```bash
$ npm install autostrada
```

## Features

- All the changes inside the www directory (files added, removed, moved, renamed, modified) are immediately taked into account.
- The files can be filtered to route only those that you want.
- You can use a custom URL for each specific file.

## Quick Start

Firstly, you must have a www directory at the root of your project. Let use the following example:

```
your project
├─ main.js
├─ node_modules
│  └─ ...
├─ package-lock.json
├─ package.json
└─ www
   ├─ index.html
   ├─ a-folder
   │  ├─ index.html
   │  └─ page.html
   └─ config.json
```

Now, you can auto route the www directory by doing:

```js
const app = require('express')();
const autostrada = require('autostrada')();

app.use(autostrada);
app.listen(80);
```

result:

| file path            | URL                                  |
|----------------------|--------------------------------------|
| /index.html          | http://localhost/index.html          |
| /a-folder/index.html | http://localhost/a-folder/index.html |
| /a-folder/page.html  | http://localhost/a-folder/page.html  |
| /config.json         | http://localhost/config.json         |

## Options

| option               | default value      |
|----------------------|--------------------|
| wwwDirectory         | __dirname + '/www' |
| indexableFilesFilter | (path) => true     |
| fromPathToUrl        | (path) => path     |

### wwwDirectory

You can choose another directory name than 'www' by providing its absolute path. Example:

```js
const autostrada = require('autostrada')({
	wwwDirectory: __dirname + '/www2'
});
```

### indexableFilesFilter

You can choose the files that will be routed by providing a filter. Example:

```js
const autostrada = require('autostrada')({
	indexableFilesFilter: (path) => path !== '/config.json'
});
```

result (the 'config.json' file is missing):

| file path            | URL                                  |
|----------------------|--------------------------------------|
| /index.html          | http://localhost/index.html          |
| /a-folder/index.html | http://localhost/a-folder/index.html |
| /a-folder/page.html  | http://localhost/a-folder/page.html  |

### fromPathToUrl

A function that associates one or many URLs at a file path. Thus, this function retrieves respectively a string or a string array. In the case where it retrieves a string array, the first one is the main URL and the others will redirect to it.

For example, if you want to get rid of the '.html' extensions and to have all the 'index.html' routed to their folder root, you can do this:

```js
const autostrada = require('autostrada')({
	fromPathToUrl: (path) => {
		// '/xxx/yyy/index.html' => ['/xxx/yyy/', '/xxx/yyy']
		if (path.match(/^((.*)\/)index\.html$/)) return [RegExp.$1, RegExp.$2];
		
		// '/xxx/yyy/zzz.html' => '/xxx/yyy/zzz'
		if (path.match(/^(.*)\.html$/)) return RegExp.$1;
		
		// no mofification
		return path;
	}
});
```

result:

| file path            | URL                            |
|----------------------|--------------------------------|
| /index.html          | http://localhost/              |
| /a-folder/index.html | http://localhost/a-folder/     |
| /a-folder/page.html  | http://localhost/a-folder/page |
| /config.json         | http://localhost/config.json   |

Also, [http://localhost]() will redirect to [http://localhost/]() and [http://localhost/a-folder]() will redirect to [http://localhost/a-folder/]().

## Last Chance

Autostrada features a function called `lastChance`:

```js
const app = require('express')();
const autostrada = require('autostrada')();

app.use(autostrada);
// your other routes here
// THEN lastChance:
app.use(autostrada.lastChance(150));
app.listen(80);
```

Normally, if no route is found, Autostrada just calls `next()`.
With `lastChance`, Autostrada keeps the request during a given amount of time, here 150ms.
If during this period a file corresponding to the request is added to the www directory, the request will be answered.
Otherwise, `next()` is called at the end of the period.

Keep in mind that if no file is found, Autostrada will wait the given amount of time before calling `next()`.
**This is a blocking operation**.

## Author

Clément Saccoccio

## Sources

[https://gitlab.com/Cl00e9ment/autostrada](https://gitlab.com/Cl00e9ment/autostrada)

## Licence

[GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.txt)
